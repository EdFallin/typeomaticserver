using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    public class UxStateTests
    {
        #region Equals()

        [Fact]
        public void Equals__NotEqual__ReturnsFalse() /* working */ {
            /* Arrange */
            UxState topic = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            UxState other = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = true,
                TypingStyle = "SomeStyle"
            };

            bool expected = false;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equals__AreEqual__ReturnsTrue() /* working */ {
            /* Arrange */
            UxState topic = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            UxState other = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            bool expected = true;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equals__DifferentClass__ReturnsFalse() /* working */ {
            /* Arrange */
            UxState topic = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            object other = new List<string>();

            bool expected = false;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Equals()


        #region GetHashCode()

        [Fact]
        public void GetHashCode__EqualContents__SameCode() /* working */ {
            /* Arrange */
            UxState topic = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            UxState other = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            int expected = topic.GetHashCode();


            /* Act */
            int actual = other.GetHashCode();


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetHashCode__DifferentContents__DifferentCodes() /* working */ {
            /* Arrange */
            UxState topic = new UxState {
                IsChooserHidden = false,
                IsTimerHidden = true,
                IsWelcomeHidden = false,
                TypingStyle = "FakeStyle"
            };

            UxState other = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = false,
                IsWelcomeHidden = true,
                TypingStyle = "DubStyle"
            };

            int expected = topic.GetHashCode();


            /* Act */
            int actual = other.GetHashCode();


            /* Assert */
            Assert.NotEqual(expected, actual);
        }

        #endregion GetHashCode()
    }
}