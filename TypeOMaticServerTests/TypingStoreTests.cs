using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Test = System.Threading.Tasks.Task;
using Try = Xunit.Assert;

using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    [Collection("Run Serially")]
    public class TypingStoreTests
    {
        #region Fixtures

        private string StoreSite /* verified */ {
            get {
                string site = Path.Combine(TypingStore.RelativeRoot, TypingStore.Folder);
                return site;
            }
        }

        private string GenerateSpoofText() /* verified */ {
            StringBuilder builder = new();

            // The text has some letters, but mostly identifies paragraph and word.
            for (int paragraph = 0; paragraph < 14; paragraph++) {
                for (int word = 0; word < 75; word++) {
                    char letter = (char)((word % 26) + 'a');
                    builder.Append($"{ letter }{ paragraph + 1 }-{word + 1} ");
                }

                // Most often, text has a blank line between paragraphs.
                builder.AppendLine();
                builder.AppendLine();
            }

            return builder.ToString();
        }

        private string GenerateDistinctSpoofText(string name, int paragraphs) /* verified */ {
            StringBuilder builder = new();

            // Generate paragraphs that start with the text's name and paragraph number, 
            // with following words being a repeated char and then the word's number.
            for (int paragraph = 0; paragraph < paragraphs; paragraph++) {
                builder.Append($"{ name }-p{ paragraph + 1 } ");
                for (int word = 1; word < 75; word++) {
                    char letter = (char)((word % 26) + 'a');

                    int repeats = word < 9 ? 3 : 2;
                    string fill = new string(letter, repeats);

                    builder.Append($"{ fill }{ word + 1 } ");
                }

                // Most often, text has a blank line between paragraphs.
                builder.AppendLine();
                builder.AppendLine();
            }

            return builder.ToString();
        }

        /// <summary>
        /// WFI = Windows File Indicator.
        /// In other words, full path, file name, and extension.
        /// </summary>
        private string CalculateWfi(string name, TypingStore subject) /* verified */ {
            string wfi = subject.Site;
            wfi = Path.Combine(wfi, name);
            wfi = Path.ChangeExtension(wfi, TypingStore.FileExt);

            return wfi;
        }

        private async Task MakeAnySite(string site) /* verified */ {
            // Giving the file system time to be ready.
            await Task.Delay(10);

            // Create the folder if it doesn't exist yet.
            if (!Directory.Exists(site)) {
                Directory.CreateDirectory(site);
            }

            // Give the file system time to catch up.
            await Task.Delay(10);
        }

        private async Task DeleteAnyFile(string wfi) /* verified */ {
            // Giving the file system time to be ready.
            await Task.Delay(10);

            // Actually delete the file if it exists.
            if (File.Exists(wfi)) {
                File.Delete(wfi);
            }

            // Give the file system time to catch up.
            await Task.Delay(10);
        }

        private async Task DeleteAnySite(string site) /* verified */ {
            // Giving the file system time to be ready.
            await Task.Delay(10);

            // Actually delete the folder if it exists, 
            // as well as any contents it may have.
            if (Directory.Exists(site)) {
                Directory.Delete(site, true);
            }

            // Give the file system time to catch up.
            await Task.Delay(10);
        }

        /// <summary>
        /// Generates the requested set of text sources, saved as files.
        /// <para/>
        /// Uses the model code method AddTextSource(), so that method 
        /// must already be passing independent tests for this to work.
        /// </summary>
        private async Task<List<TextSource>> SetUpKnownTextSources(TypingStore store, string nameRoot, int[] lengths) /* verified */ {
            // Giving the file system time to catch up.
            await Task.Delay(10);

            List<TextSource> knowns = new();

            // Generating the files using another fixture.
            for (int at = 0; at < lengths.Length; at++) {
                string name = $"{ nameRoot }-{ at + 1 }";
                string text = GenerateDistinctSpoofText(name, lengths[at]);

                TextSource toMake = new(name, text);
                store.AddTextSource(toMake);

                knowns.Add(toMake);
            }

            // Giving the file system time to catch up.
            await Task.Delay(10);

            return knowns;
        }

        private async Task TearDownAnyTextSources(string site) /* verified */ {
            await DeleteAnySite(site);
        }

        #endregion Fixtures


        #region TypingStore()

        [Fact]
        public void Constructor__NoConditions__RightStorePathAssembled() /* working */ {
            /* Arrange */
            string appFolder = TypingStore.RelativeRoot;
            string stateFolder = TypingStore.Folder;

            string expected = Path.Combine(appFolder, stateFolder);


            /* Act */
            TypingStore store = new();


            /* Assemble */
            string actual = store.Site;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test Constructor__NoSiteExists__SiteCreated() /* working */ {
            /* Arrange */
            // Ensuring the site doesn't exist.
            await DeleteAnySite(StoreSite);


            /* Pre-testing */
            Try.False(Directory.Exists(StoreSite));

            bool expected = true;


            /* Act */
            TypingStore subject = new TypingStore();
            bool actual = Directory.Exists(subject.Site);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test Constructor__SiteExists__NoThrowAndSiteExists() /* working */ {
            /* Arrange */
            // This should create the site.
            TypingStore subject = new TypingStore();
            string site = subject.Site;


            /* Pre-testing */
            Try.True(Directory.Exists(site));

            Exception expected = null;
            Exception actual = null;

            bool expDoesExist = true;


            /* Act */
            try {
                subject = new TypingStore();
            }
            catch (Exception x) {
                actual = x;
            }


            /* Gathering results */
            bool acDoesExist = Directory.Exists(site);


            /* Clean up */
            await DeleteAnySite(site);


            /* Assert */
            Try.Equal(expected, actual);
            Try.Equal(expDoesExist, acDoesExist);
        }

        #endregion TypingStore()


        #region AddTextSource()

        [Fact]
        public async Test AddTextSource__KnownTextSource__FileSavedWithContents() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            string name = "SpoofTextSource";
            string expText = GenerateSpoofText();
            TextSource toSave = new TextSource(name, expText);

            string wfi = CalculateWfi(name, subject);
            await DeleteAnyFile(wfi);


            /* Act */
            subject.AddTextSource(toSave);


            /* Assemble */
            string acText = File.ReadAllText(wfi);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Equal(expText, acText);
        }

        #endregion AddTextSource()


        #region RemoveTextSource()

        [Fact]
        public async Test RemoveTextSource__KnownSource__IsDeleted() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            string name = "SourceToRemove";
            TextSource target = new(name, "short text");

            string wfi = CalculateWfi(name, subject);

            // Isolate state.
            await DeleteAnyFile(wfi);

            // Create the file to delete.
            subject.AddTextSource(target);

            // Give the file system time to catch up.
            await Task.Delay(10);


            /* Pre-test. */
            Try.True(File.Exists(wfi));


            /* Act */
            subject.RemoveTextSource(name);


            /* Assemble */
            bool actual = File.Exists(wfi);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.False(actual);
        }

        [Fact]
        public async Test RemoveTextSource__NoSuchSource__NoThrow() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            string name = "FileThatDoesNotExist";
            TextSource target = new(name, "short text");

            string wfi = CalculateWfi(name, subject);

            // Isolate state.
            await DeleteAnyFile(wfi);


            /* Pre-test. */
            Try.False(File.Exists(wfi));

            Exception actual = null;


            /* Act */
            try {
                subject.RemoveTextSource(name);
            }
            catch (Exception x) {
                actual = x;
            }


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        #endregion RemoveTextSource()


        #region RandomTypeableByTime()

        [Fact]
        public async Test RandomTypeableByTime__KnownTypeables__ReturnsRightLengthTypeable() /* working */ {
            /* Arrange */

            string nameRoot = "Test-Random-Length";
            int[] lengths = { 6, 8, 10 };

            // Isolating test, with known text sources and :. typeables.
            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            await SetUpKnownTextSources(subject, nameRoot, lengths);

            int expClaimedLength = 3;
            int expMinChars = 3 * 100 * 5;
            int expMaxChars = 5 * 100 * 5;


            /* Act */
            Typeable actual = subject.RandomTypeableByTime(3);


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            Try.Equal(expClaimedLength, actual.MinuteLength);
            Try.True(actual.Text.Length >= expMinChars);
            Try.True(actual.Text.Length < expMaxChars);
        }

        [Fact]
        public async Test RandomTypeableByTime__KnownTypeablesRepeatCalls__DifferentTypeablesReturned() /* working */ {
            /* Arrange */
            // Isolating test, with one known text source with just enough typeables.
            string nameRoot = "Test-Random-Repeat";
            int[] lengths = { 10 };

            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            await SetUpKnownTextSources(subject, nameRoot, lengths);


            /* Act */
            List<Typeable> actuals = new();

            for (int at = 0; at < 25; at++) {
                Typeable actual = subject.RandomTypeableByTime(3);
                actuals.Add(actual);
            }


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            // A Typeable should not repeat the incipit text 
            // of any of the last three Typeables returned.
            for (int of = 3; of < actuals.Count; of++) {
                Typeable actual = actuals[of];
                Typeable expNotMinus1 = actuals[of - 1];
                Typeable expNotMinus2 = actuals[of - 2];
                Typeable expNotMinus3 = actuals[of - 3];

                // .Name is the incipit for each Typeable.
                Try.NotEqual(actual.Name, expNotMinus1.Name);
                Try.NotEqual(actual.Name, expNotMinus2.Name);
                Try.NotEqual(actual.Name, expNotMinus3.Name);
            }
        }

        [Fact]
        public async Test RandomTypeableByTime__RepeatCallsDifferentLengths_IncipitNamesRecentlyUnique() /* working */ {
            /* Arrange */
            // Isolating test, with one known text source with just enough typeables.
            string nameRoot = "Test-Random-Varying";
            int[] lengths = { 12 };

            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            await SetUpKnownTextSources(subject, nameRoot, lengths);


            /* Act */
            List<Typeable> actuals = new();
            int[] times = { 1, 2, 3, 5 };

            for (int at = 0; at < 16; at++) {
                int of = at % times.Length;
                Typeable actual = subject.RandomTypeableByTime(times[of]);
                actuals.Add(actual);
            }


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            // A Typeable should not repeat the incipit text of any 
            // of the last three Typeables returned, of any length.
            for (int of = 3; of < actuals.Count; of++) {
                Typeable actual = actuals[of];
                Typeable expNotMinus1 = actuals[of - 1];
                Typeable expNotMinus2 = actuals[of - 2];
                Typeable expNotMinus3 = actuals[of - 3];

                // .Name is the incipit for each Typeable.
                Try.NotEqual(actual.Name, expNotMinus1.Name);
                Try.NotEqual(actual.Name, expNotMinus2.Name);
                Try.NotEqual(actual.Name, expNotMinus3.Name);
            }
        }

        [Fact]
        public async Test RandomTypeableByTime__NoTextSources__ReturnsNull() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            // Isolating test, with folder present, but no text sources at all.
            await TearDownAnyTextSources(subject.Site);
            await MakeAnySite(subject.Site);


            /* Act */
            Typeable actual = subject.RandomTypeableByTime(3);


            // No cleanup needed: no files.


            /* Assert */
            Try.Null(actual);
        }

        [Fact]
        public async Test RandomTypeableByTime__NotEnoughTextSourcesRepeatCalls__EventuallyReturnsNulls() /* working */ {
            /* Arrange */

            // Isolating test, with one text source with just enough 
            // text for 3 distinct typeables of the target length.
            string nameRoot = "Test-Random-Few";
            int minutes = 3;
            int[] lengths = { 6 };

            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            await SetUpKnownTextSources(subject, nameRoot, lengths);


            /* Act */
            List<Typeable> actuals = new();

            for (int at = 0; at < 25; at++) {
                Typeable actual = subject.RandomTypeableByTime(3);
                actuals.Add(actual);
            }


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            // The first 3 should be Typeables.
            Try.NotNull(actuals[0]);
            Try.NotNull(actuals[1]);
            Try.NotNull(actuals[2]);

            // After the first few, all others should be nulls, 
            // because all the possible have been recently used.
            for (int of = 3; of < actuals.Count; of++) {
                Try.Null(actuals[of]);
            }
        }

        [Fact]
        public async Test RandomTypeableByTime__NoTextSourceLongEnoughForTime__ReturnsNull() /* working */ {
            /* Arrange */
            string nameRoot = "Test-Short";

            // 10 minutes = 5000 chars, more than 6 paragraphs contain.
            int minutes = 10;
            int[] lengths = { 6 };

            // Isolating test, with known text source and :. typeables.
            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            await SetUpKnownTextSources(subject, nameRoot, lengths);


            /* Act */
            Typeable actual = subject.RandomTypeableByTime(minutes);


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        #endregion RandomTypeableByTime()


        #region AllTextNames()

        [Fact]
        public async Test AllTextNames__NoTextFiles__ReturnsEmptyList() /* working */ {
            /* Arrange */
            // Isolating test.  Model code ctor creates site if needed.
            await DeleteAnySite(StoreSite);

            TypingStore subject = new();


            /* Act */
            List<string> actual = subject.AllTextNames();


            /* Assert */
            Try.Empty(actual);
        }

        [Fact]
        public async Test AllTextNames__KnownTextFiles__ReturnsMatchingNames() /* working */ {
            /* Arrange */
            // Isolating test.  Model code ctor creates site if needed.
            await DeleteAnySite(StoreSite);

            TypingStore subject = new();

            // Producing the names and their files to be found.
            List<string> expecteds = new() { "File A", "File C", "File B" };

            foreach (string expected in expecteds) {
                string text = GenerateDistinctSpoofText(expected, 6);
                subject.AddTextSource(new TextSource(expected, text));
            }


            // The method's output is sorted.
            expecteds.Sort();


            /* Act */
            object actuals = subject.AllTextNames();


            /* Clean up */
            await DeleteAnySite(StoreSite);


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        #endregion AllTextNames()


        #region AllTexts()

        [Fact]
        public async Test AllTexts__KnownFiles__CorrectTextSources() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            List<TextSource> sources = new List<TextSource> {
                new("Nearly Empty 1", "1"),
                new("Almost Empty 2", "2"),
                new("Close To Empty 3", "3")
            };

            // Making a file for each TextSource.
            foreach (TextSource source in sources) {
                subject.AddTextSource(source);
            }

            // Converting to tuples for easier comparing later.
            List<(string, string)> expecteds = sources
                .OrderBy(x => x.Name)
                .Select(x => (x.Name, x.Text))
                .ToList();

            // New subject to ensure isolation always.
            subject = new();


            /* Act */
            List<TextSource> acTextSources = subject.AllTexts();


            /* Gather results */
            // Converting to tuples for easier comparing later.
            List<(string, string)> actuals = acTextSources
                .OrderBy(x => x.Name)
                .Select(x => (x.Name, x.Text))
                .ToList();


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        [Fact]
        public async Test AllTexts__NoFiles__ReturnsEmptyCollection() /* working */ {
            /* Arrange */
            // Isolating test with no files present.
            // Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();


            /* Act */
            List<TextSource> actuals = subject.AllTexts();


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Empty(actuals);
        }

        #endregion AllTexts()


        #region AllTypeables()

        [Fact]
        public async Test AllTypeables__KnownFiles__AllCorrectTypeables() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            (string name1, string name2, string name3)
                = ("All-Typeables-1", "All-Typeables-2", "All-Typeables-3");

            List<TextSource> sources = new List<TextSource> {
                new(name1, GenerateDistinctSpoofText(name1, 6)  ),
                new(name2, GenerateDistinctSpoofText(name2, 16)),
                new(name3, GenerateDistinctSpoofText(name3, 34))
            };

            List<Typeable> expecteds = sources
                .SelectMany(x => x.AsTypeables())
                .OrderBy(x => x.Name)
                .ThenBy(x => x.MinuteLength)
                .ToList();

            // Making a file for each TextSource.
            foreach (TextSource source in sources) {
                subject.AddTextSource(source);
            }

            // New subject to ensure isolation always.
            subject = new();


            /* Act */
            List<Typeable> actuals = subject.AllTypeables();

            actuals = actuals.OrderBy(x => x.Name)
                .ThenBy(x => x.MinuteLength)
                .ToList();


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        [Fact]
        public async Test AllTypeables__NoFiles__ReturnsEmptyCollection() /* working */ {
            /* Arrange */
            // Isolating test with no files present.
            // Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();


            /* Act */
            List<Typeable> actuals = subject.AllTypeables();


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Empty(actuals);
        }

        #endregion AllTypeables()


        #region TypeableByNameAndTime()

        [Fact]
        public async Test TypeableByNameAndTime__NameAndTimeExist__ReturnsTypeableFromNamedSource() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            // Making the TextSource file with enough paragraphs.
            string name = "Test-Name-And-Time-From-Source";
            string text = GenerateDistinctSpoofText(name, 10);

            TextSource source = new(name, text);
            subject.AddTextSource(source);


            // New subject to ensure isolation always.
            subject = new();


            // Any of these might be the output.
            List<Typeable> expecteds = source.AsTypeables();


            /* Act */
            Typeable actual = subject.TypeableByNameAndTime(name, 3);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Contains(actual, expecteds);
        }

        [Fact]
        public async Test TypeableByNameAndTime__NameAndTimeExist__ReturnsRightLengthTypeable() /* working */ {
            /* Arrange */
            string name = "Test-Name-And-Time-Length";

            // Isolating test, with known text sources and :. typeables.
            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            string text = GenerateDistinctSpoofText(name, 8);
            subject.AddTextSource(new TextSource(name, text));

            int expClaimedLength = 3;
            int expMinChars = 3 * 100 * 5;
            int expMaxChars = 5 * 100 * 5;

            // Fully isolating model code under test.
            subject = new();


            /* Act */
            Typeable actual = subject.TypeableByNameAndTime(name, 3);


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            Try.Equal(expClaimedLength, actual.MinuteLength);
            Try.True(actual.Text.Length >= expMinChars);
            Try.True(actual.Text.Length < expMaxChars);
        }

        [Fact]
        public async Test TypeableByNameAndTime__SeveralCallsNameAndTimeExist__DifferentTypeablesReturned() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            // Making the TextSource file with enough paragraphs.
            string name = "Test-Repeat-Name-And-Time";
            string text = GenerateDistinctSpoofText(name, 12);

            TextSource source = new(name, text);
            subject.AddTextSource(source);

            // New subject to ensure isolation always.
            subject = new();


            /* Act */
            List<Typeable> actuals = new();

            for (int at = 0; at < 25; at++) {
                Typeable actual = subject.TypeableByNameAndTime(name, 3);
                actuals.Add(actual);
            }


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            // A Typeable should not repeat the incipit text 
            // of any of the last three Typeables returned.
            for (int of = 3; of < actuals.Count; of++) {
                Typeable actual = actuals[of];
                Typeable expNotMinus1 = actuals[of - 1];
                Typeable expNotMinus2 = actuals[of - 2];
                Typeable expNotMinus3 = actuals[of - 3];

                // .Name is the incipit for each Typeable.
                Try.NotEqual(actual.Name, expNotMinus1.Name);
                Try.NotEqual(actual.Name, expNotMinus2.Name);
                Try.NotEqual(actual.Name, expNotMinus3.Name);
            }

        }

        [Fact]
        public async Test TypeableByNameAndTime__RepeatCallsDifferentLengths__IncipitNamesRecentlyUnique() /* working */ {
            /* Arrange */
            // Isolating test, with one known text source with just enough typeables.
            string name = "Test-Name-And-Time-Varying";

            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            string text = GenerateDistinctSpoofText(name, 24);
            subject.AddTextSource(new TextSource(name, text));

            // Fully isolating model code instance.
            subject = new();


            /* Act */
            List<Typeable> actuals = new();
            int[] times = { 1, 2, 10, 3, 5 };

            for (int at = 0; at < 16; at++) {
                int of = at % times.Length;
                Typeable actual = subject.TypeableByNameAndTime(name, times[of]);
                actuals.Add(actual);
            }


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            // A Typeable should not repeat the incipit text of any 
            // of the last three Typeables returned, of any length.
            for (int of = 3; of < actuals.Count; of++) {
                Typeable actual = actuals[of];
                Typeable expNotMinus1 = actuals[of - 1];
                Typeable expNotMinus2 = actuals[of - 2];
                Typeable expNotMinus3 = actuals[of - 3];

                // .Name is the incipit for each Typeable.
                Try.NotEqual(actual.Name, expNotMinus1.Name);
                Try.NotEqual(actual.Name, expNotMinus2.Name);
                Try.NotEqual(actual.Name, expNotMinus3.Name);
            }
        }

        [Fact]
        public async Test TypeableByNameAndTime__NotEnoughTextSourcesRepeatCalls__EventuallyReturnsNulls() /* working */ {
            /* Arrange */
            // Isolating test, with one text source with just enough 
            // text for 3 distinct typeables of the target length.
            string name = "Test-Name-And-Time-Few";

            await TearDownAnyTextSources(StoreSite);

            TypingStore subject = new();
            string text = GenerateDistinctSpoofText(name, 6);
            subject.AddTextSource(new TextSource(name, text));


            // Fully isolating model code instance just in case.
            subject = new();


            /* Act */
            List<Typeable> actuals = new();

            for (int at = 0; at < 25; at++) {
                Typeable actual = subject.TypeableByNameAndTime(name, 3);
                actuals.Add(actual);
            }


            /* Clean up */
            await TearDownAnyTextSources(subject.Site);


            /* Assert */
            // The first 3 should be Typeables.
            Try.NotNull(actuals[0]);
            Try.NotNull(actuals[1]);
            Try.NotNull(actuals[2]);

            // After the first few, all others should be nulls, 
            // because all the possible have been recently used.
            for (int of = 3; of < actuals.Count; of++) {
                Try.Null(actuals[of]);
            }
        }

        [Fact]
        public async Test TypeableByNameAndTime__NoSuchNamedFile__ReturnsNull() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            // Naming a TextSource that doesn't exist, since the site was just re-created.
            string name = "Test-No-Such-Name-And-Time";
            string wfi = CalculateWfi(name, subject);


            /* Pre-test */
            Try.False(File.Exists(wfi));


            // New subject to ensure isolation always.
            subject = new();


            /* Act */
            Typeable actual = subject.TypeableByNameAndTime(name, 3);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        [Fact]
        public async Test TypeableByNameAndTime__NoSuchTimeTypeable__ReturnsNull() /* working */ {
            /* Arrange */
            // Isolating test.  Constructor creates site if needed.
            await DeleteAnySite(StoreSite);
            TypingStore subject = new();

            // Making the TextSource file too few paragraphs for the time chosen.
            string name = "Test-No-Such-Time-By-Name";
            string text = GenerateDistinctSpoofText(name, 3);

            TextSource source = new(name, text);
            subject.AddTextSource(source);


            // New subject to ensure isolation always.
            subject = new();


            // Any of these might be the output.
            List<Typeable> expecteds = source.AsTypeables();


            /* Act */
            // Five minutes requires more than three spoof paragraphs.
            Typeable actual = subject.TypeableByNameAndTime(name, 5);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        #endregion TypeableByNameAndTime()


        #region TextSourceByName()

        [Fact]
        public async Test TextSourceByName__NoSuchFile__ReturnsNull() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            // Ensuring no target file exists.
            string name = "NoSourceFromName";
            string wfi = CalculateWfi(name, subject);
            await DeleteAnyFile(wfi);


            /* Act */
            TextSource actual = subject.TextSourceByName(name);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        [Fact]
        public async Test TextSourceByName__KnownFile__ReturnsCorrectTextSource() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            string name = "SourceFromName";
            string expText = GenerateSpoofText();
            TextSource toSave = new TextSource(name, expText);

            string wfi = CalculateWfi(name, subject);
            await DeleteAnyFile(wfi);

            subject.AddTextSource(toSave);

            TextSource expected = toSave;


            /* Act */
            TextSource actual = subject.TextSourceByName(name);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            // If these two properties are equal, the TextSources are equal.
            Try.Equal(expected.Name, actual.Name);
            Try.Equal(expected.Text, actual.Text);
        }

        #endregion TextSourceByName()


        #region TextSourceByWfi()

        [Fact]
        public async Test TextSourceByWfi__NoSuchFile__ReturnsNull() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            // Ensuring no target file exists.
            string name = "NoSourceFromWfi";
            string wfi = CalculateWfi(name, subject);
            await DeleteAnyFile(wfi);


            /* Act */
            TextSource actual = subject.TextSourceByWfi(wfi);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            Try.Null(actual);
        }

        [Fact]
        public async Test TextSourceByWfi__KnownFile__ReturnsCorrectTextSource() /* working */ {
            /* Arrange */
            TypingStore subject = new();

            string name = "SourceFromWfi";
            string expText = GenerateSpoofText();
            TextSource toSave = new TextSource(name, expText);

            string wfi = CalculateWfi(name, subject);
            await DeleteAnyFile(wfi);

            subject.AddTextSource(toSave);

            TextSource expected = toSave;


            /* Act */
            TextSource actual = subject.TextSourceByWfi(wfi);


            /* Clean up */
            await DeleteAnySite(subject.Site);


            /* Assert */
            // If these two properties are equal, the TextSources are equal.
            Try.Equal(expected.Name, actual.Name);
            Try.Equal(expected.Text, actual.Text);
        }

        #endregion TextSourceByWfi()

    }
}