using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

using Test = System.Threading.Tasks.Task;

using Microsoft.AspNetCore.Mvc;
using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    [Collection("Run Serially")]
    public class UxStateControllerTests
    {
        #region Fixture types

        private class _UxStateStore : IUxStateStore
        {
            #region Fields

            private bool _doThrow;
            private UxState _state;

            #endregion Fields


            #region Constructors

            public _UxStateStore(bool doThrow) : this(doThrow, null) {
                /* no operations */
            }

            public _UxStateStore(bool doThrow, UxState state) {
                _doThrow = doThrow;
                _state = state;
            }

            #endregion Constructors


            #region IUxStateStore

            public UxState State {
                get {
                    if (_doThrow) {
                        throw new Exception("Spoofed fail");
                    }

                    return _state;
                }
                set {
                    _state = value;
                }
            }

            public void Save() {
                if (_doThrow) {
                    throw new Exception("Spoofed fail.");
                }
            }

            #endregion IUxStateStore
        }

        #endregion Fixture types


        #region Fixtures

        private void DeleteUxStateFile(UxStateStore store) /* verified */ {
            string target = store.Subject;
            // target = Path.GetDirectoryName(target);

            if (File.Exists(target)) {
                File.Delete(target);
            }
        }

        #endregion Fixtures


        #region Create()

        [Fact]
        public async Test Create__KnownUxState__ProvidedUxStateIsStored() /* working */ {
            /* Create() retains on the store class and saves to a file. */

            /* Arrange */
            UxStateStore saver = new();
            DeleteUxStateFile(saver);

            UxState state = new();
            state.IsTimerHidden = true;
            state.TypingStyle = "SomeStyle";

            saver.State = state;
            saver.Save();

            // Using distinct store instances to isolate store during call.
            UxStateStore store = new();
            UxStateController topic = new(store);

            UxState expected = new();
            expected.IsChooserHidden = true;
            expected.TypingStyle = "FakeStyle";


            /* Act */
            await topic.Create(expected);


            /* Assemble */
            // This store should now retain the new / expected state.
            UxStateStore retriever = new();
            UxState actual = retriever.State;


            /* Clean up */
            DeleteUxStateFile(retriever);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Create__SuccessfulSave__ReturnsOkStatus() /* working */ {
            /* Create() returns a 200 (OK) and no data if the save succeeds. */

            /* Arrange */
            bool doThrowOnSave = false;
            IUxStateStore store = new _UxStateStore(doThrowOnSave);

            UxStateController topic = new(store);
            UxState unused = new();

            // HTTP OK code.
            int expected = 200;


            /* Act */
            IActionResult raw = await topic.Create(unused);
            OkResult result = raw as OkResult;
            int actual = result.StatusCode;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Create__SaveFails__ReturnsErrorStatus() /* working */ {
            /* Create() returns a 500 (server-end error) 
               and no data if the save fails. */

            /* Arrange */
            bool doThrowOnSave = true;
            IUxStateStore store = new _UxStateStore(doThrowOnSave);

            UxStateController topic = new(store);
            UxState unused = new();

            // HTTP server-end-error code.
            int expected = 500;


            /* Act */
            IActionResult raw = await topic.Create(unused);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Create()


        #region Get()

        [Fact]
        public async Test Get__RequestSucceeds__ReturnIncludesOkStatus() /* working */ {
            /* Arrange */
            UxState state = new();

            bool doThrowOnStateGet = false;
            IUxStateStore store = new _UxStateStore(doThrowOnStateGet, state);
            UxStateController topic = new(store);

            // HTTP OK status.
            int expected = 200;


            /* Act */
            IActionResult raw = await topic.Get();
            ObjectResult result = raw as ObjectResult;
            int actual = result.StatusCode.Value;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Get__RequestFails__ReturnIncludesErrorStatus() /* working */ {
            /* Arrange */
            UxState state = new();

            bool doThrowOnStateGet = true;
            IUxStateStore store = new _UxStateStore(doThrowOnStateGet);
            UxStateController topic = new(store);

            // HTTP OK status.
            int expected = 500;


            /* Act */
            IActionResult raw = await topic.Get();
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Get__KnownStateProvided__SameStateReturned() /* working */ {
            /* Arrange */
            UxState state = new UxState {
                IsChooserHidden = true,
                TypingStyle = "Styled"
            };

            UxState expected = state;

            bool doThrowOnStateGet = false;
            IUxStateStore store = new _UxStateStore(doThrowOnStateGet, state);
            UxStateController topic = new(store);


            /* Act */
            IActionResult raw = await topic.Get();
            ObjectResult result = raw as ObjectResult;
            UxState actual = result.Value as UxState;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Get__KnownStoredUxState__SameUxStateReturned() /* working */ {
            /* Arrange */
            UxState u = new();

            u.IsChooserHidden = u.IsTimerHidden = u.IsWelcomeHidden = true;
            u.TypingStyle = "StylishTyping";

            UxState expected = u;

            UxStateStore saver = new();
            DeleteUxStateFile(saver);

            saver.State = expected;
            saver.Save();

            // Using distinct store instances to isolate store during call.
            UxStateStore store = new();
            UxStateController topic = new(store);


            /* Act */
            IActionResult raw = await topic.Get();
            ObjectResult result = raw as ObjectResult;
            UxState actual = result.Value as UxState;


            /* Clean up */
            DeleteUxStateFile(saver);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Get()
    }
}
