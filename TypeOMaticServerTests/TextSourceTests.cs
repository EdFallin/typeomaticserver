using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;

using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    public class TextSourceTests
    {
        #region Fixtures

        private string GenerateSpoofTextWithBlanksBetweenParagraphs() /* verified */ {
            StringBuilder builder = new();

            // The text has some letters, but mostly identifies paragraph and word.
            for (int paragraph = 0; paragraph < 14; paragraph++) {
                for (int word = 0; word < 75; word++) {
                    char letter = (char)((word % 26) + 'a');
                    builder.Append($"{ letter }{ paragraph + 1 }-{word + 1} ");
                }

                // Real text often has two hard returns between paragraphs.
                builder.AppendLine();
                builder.AppendLine();
            }

            return builder.ToString();
        }

        private string GenerateSpoofTextNoBlanksBetweenParagraphs() /* verified */ {
            StringBuilder builder = new();

            // The text has some letters, but mostly identifies paragraph and word.
            for (int paragraph = 0; paragraph < 14; paragraph++) {
                for (int word = 0; word < 75; word++) {
                    char letter = (char)((word % 26) + 'a');
                    builder.Append($"{ letter }{ paragraph + 1 }-{word + 1} ");
                }

                // Real text sometimes has only one hard return between paragraphs.
                builder.AppendLine();
            }

            return builder.ToString();
        }

        private List<Typeable> ConvertTextToTypeables(string name, string text) /* verified */ {
            List<Typeable> typeables = new();

            StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries;
            string[] paragraphs = text.Split(Environment.NewLine, options);

            // 100 words a minute, each word a 5-char stretch.
            int[] totals = { 500, 1000, 1500, 2500, 5000 };

            // Trying to build all lengths possible from each paragraph.
            for (int p = 0; p < paragraphs.Length; p++) {
                // Building all possible lengths starting at the current paragraph.
                foreach (int total in totals) {
                    // First paragraph is always included.
                    StringBuilder b = new(paragraphs[p]);
                    int n = p + 1;

                    // Later paragraphs added until segment passes target length.
                    while (b.Length < total && n < paragraphs.Length) {
                        b.Append(' ');
                        b.Append(paragraphs[n]);
                        n++;
                    }

                    // If not as much text as targeted, the segment is skipped.
                    if (b.Length < total) {
                        break;
                    }

                    // From text segment to Typeable with incipit as name.
                    Typeable t = new(b.ToString());
                    typeables.Add(t);
                }
            }

            return typeables;
        }

        #endregion Fixtures


        #region AsTypeables()

        [Fact]
        public void AsTypeables__KnownTextBlanksBetweenParagraphs__CorrectTypeables() /* working */ {
            /* Arrange */
            string name = "Name";
            string text = GenerateSpoofTextWithBlanksBetweenParagraphs();

            List<Typeable> expecteds = ConvertTextToTypeables(name, text);

            TextSource topic = new(name, text);


            /* Act */
            List<Typeable> actuals = topic.AsTypeables();


            /* Assert */
            Assert.Equal(expecteds, actuals);
        }

        [Fact]
        public void AsTypeables__KnownTextNoBlanksBetweenParagraphs__CorrectTypeables() /* working */ {
            /* Arrange */
            string name = "Name";
            string text = GenerateSpoofTextNoBlanksBetweenParagraphs();

            List<Typeable> expecteds = ConvertTextToTypeables(name, text);

            TextSource topic = new(name, text);


            /* Act */
            List<Typeable> actuals = topic.AsTypeables();


            /* Assert */
            Assert.Equal(expecteds, actuals);
        }

        #endregion AsTypeables()

    }
}