using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

using Test = System.Threading.Tasks.Task;

using Microsoft.AspNetCore.Mvc;
using TypeOMaticServer;
using Xunit;
using Moq;

using Try = Xunit.Assert;

namespace TypeOMaticServerTests
{
    [Collection("Run Serially")]
    public class TypingControllerTests
    {
        #region GetMinutesPossible()

        [Fact]
        public async Test GetMinutesPossible__StoreReturnsKnownMinutes__ReturnsOkWithSameMinutes() /* working */ {
            /* Arrange */
            int[] expected = { 1, 2, 3, 7, 10, 15 };

            Mock<ITypingStore> spoofer = new();

            spoofer.SetupGet(x => x.AllowedTimes)
                .Returns(expected);

            TypingController topic = new(spoofer.Object);


            /* Act */
            IActionResult raw = await topic.GetMinutesPossible();
            OkObjectResult result = raw as OkObjectResult;
            int[] actual = result.Value as int[];


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetMinutesPossible__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            spoofer.SetupGet(x => x.AllowedTimes)
                .Throws(new Exception("Test Exception"));

            TypingController topic = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.GetMinutesPossible();
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetMinutesPossible()


        #region GetRandomPieceByTime()

        [Fact]
        public async Test GetRandomPieceByTime__StoreReturnsTypeable__ReturnsOkObjectResultWithTypeable() /* working */ {
            /* Arrange */
            Typeable expected = new("Spoof", "Spoof text");

            // Initing a spoof factory for the store interface.
            Mock<ITypingStore> spoofer = new();

            // Telling the factory how to spoof the dependency method.
            spoofer.Setup(
                // In this lambda, x is the ITypingStore.
                x => x.RandomTypeableByTime(
                    // When any integer is provided as the sole arg, then...
                    // IsAny() is a method; similar others take lambdas or 
                    // Funcs as args to define conditional spoofed behavior.
                    It.IsAny<int>())
                )
                // The return value of Setup() is a definition for how to handle 
                // the spoofed method, which is extended here by defining 
                // what the spoofed method should return, either conditionally 
                // (arranged using Setup() choices) or always (as done here).
                .Returns(expected);

            // Also mocking the minutes possible to include the one provided.
            spoofer.SetupGet(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // The model code.  The dependency instance to inject 
            // is provided by the spoof factory's .Object property.
            TypingController topic = new TypingController(spoofer.Object);


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByTime(3);
            OkObjectResult result = raw as OkObjectResult;
            Typeable actual = result.Value as Typeable;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByTime__StoreReturnsNull__Returns404NotFoundError() /* working */ {
            /* Arrange */
            // Initing the spoof generator...
            Mock<ITypingStore> spoofer = new();

            // And telling it what to generate for the dependency method...
            spoofer.Setup(
                // Telling it what to have the spoof ITypingStore method do.
                x => x.RandomTypeableByTime(
                    // The spoof method should do this regardless of the arg.
                    It.IsAny<int>()
                    )
                )
                // The spoof method should always return null here.
                .Returns<Typeable>(null);

            // Also mocking the minutes possible to include the one provided.
            spoofer.SetupGet(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Initing the model code instance with the generated spoof .Object.
            TypingController topic = new(spoofer.Object);

            int expected = 404;


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByTime(2);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByTime__StoreThrowsException__Returns500Error() /* working */ {
            /* Arrange */
            // Spoof generator.
            Mock<ITypingStore> spoof = new();

            // Defining spoofed method on x as spoofed instance.
            // Result is not conditional when using It.IsAny().
            // Throws() on definition defines result of call.
            spoof.Setup(
                x => x.RandomTypeableByTime(
                    It.IsAny<int>()
                    )
                )
                .Throws<Exception>();

            // Also mocking the minutes possible to include the one provided.
            spoof.SetupGet(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // The model code, provided .Object as spoofed dependency.
            TypingController topic = new(spoof.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByTime(5);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByTime__KnownArg__StoreGivenSameArg() /* working */ {
            /* Arrange */
            int expected = 3;

            // Initing spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Setting up the method on the spoof object x.
            spoofer.Setup(
                // Applying an Is() arg condition with a lambda.  The arg must equal expected.
                x => x.RandomTypeableByTime(It.Is<int>(x => x == expected))
                )
                // The conditions set up can be checked later.  A fail returns the given message.
                .Verifiable("The expected arg was not provided to the dependency.");

            // Also mocking the minutes possible to include the one provided.
            spoofer.SetupGet(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Initing the model code with .Object, the actual spoof.
            TypingController topic = new(spoofer.Object);


            /* Act */
            await topic.GetRandomPieceByTime(3);


            /* Assert */
            // Use the spoofer's feature to see if arg conditions were met.
            spoofer.Verify();
        }

        [Fact]
        public async Test GetRandomPieceByTime__MinutesArgNotValid__Returns400BadRequest() /* working */ {
            Mock<ITypingStore> spoofer = new();

            // Spoofed property returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Spoofed method should never be called; if it is, it always throws.
            spoofer.Setup(x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>()))
                .Throws(new Exception("This method should never be reached."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetRandomPieceByTime(8);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetRandomPieceByTime()


        #region GetRandomPieceByNameAndTime()

        [Fact]
        public async Test GetRandomPieceByNameAndTime__StoreReturnsTypeable__ReturnsOkObjectResultWithTypeable() /* working */ {
            /* Arrange */
            Typeable expected = new("Spoof Name", "Spoof text with just a few words.");

            // Making a spoof generator for the dependency.
            Mock<ITypingStore> spoofer = new();

            // Spoofed property on x returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Setting up the spoofed method on x by first defining any conditions 
            // for its args (here, for any arg), then what it returns given those args.
            spoofer.Setup(
                    // Here, the spoofed method is set up non-conditionally, with IsAny().
                    x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>())
                )
                // Here, since the spoofing is non-conditional, it always returns this.
                .Returns(expected);

            // Initing the model code with .Object, the actual generated spoof.
            TypingController topic = new(spoofer.Object);


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByNameAndTime("Spoof Name", 1);
            OkObjectResult result = raw as OkObjectResult;
            Typeable actual = result.Value as Typeable;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__StoreReturnsNull__Returns404NotFoundError() /* working */ {
            /* Arrange */
            // Spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Defining spoofed property with real values.  Spoof is x here.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Defining spoof method.  Spoof is x here, later .Object.
            spoofer.Setup(
                    // Defining args, not conditional here.
                    x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>())
                )
                // Defining return value given these args.
                .Returns<Typeable>(null);

            // Initing the model code, and using the actual spoof.
            TypingController topic = new(spoofer.Object);

            int expected = 404;


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByNameAndTime("Spoof Name", 3);
            NotFoundResult result = raw as NotFoundResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__StoreThrowsException__Returns500Error() /* working */ {
            /* Arrange */
            // Spoof generator and setup.
            Mock<ITypingStore> spoofer = new();

            // Spoofed property on x returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            spoofer.Setup(
                    // On the spoof x, set up the method so it acts the same for any args.
                    x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>())
                )
                // What the method does with any args is throw this Exception.
                .Throws(new Exception("Any throw"));


            // Initing model code with actual spoof.
            TypingController topic = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.GetRandomPieceByNameAndTime("Spoof Name", 5);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__KnownArgs__StoreGivenSameArgs() /* working */ {
            /* Arrange */
            string expName = "Spoof Text Name";
            int expMinutes = 2;

            // Initing the spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Setting up the spoofed property to return the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Setting up the spoofed method to expect the given args.
            spoofer.Setup(
                    // On the spoof x, the method should get the two expected values as args.
                    x => x.TypeableByNameAndTime(
                        // Each arg has a lambda defining whether it is the right arg or not.
                        It.Is<string>(x => x == expName), It.Is<int>(x => x == expMinutes)
                    )
                )
                // A method can be called to see if the conditions set up were met.
                // If they were not, the message here is output in the test's fail.
                .Verifiable("The expected arguments were not provided to the dependency.");

            // Initing the model code with .Object, the actual spoof.
            TypingController topic = new(spoofer.Object);


            /* Act */
            await topic.GetRandomPieceByNameAndTime(expName, expMinutes);


            /* Assert */
            // Use the spoof generator's feature to see if the conditions 
            // set up were met, in this case whether the args were right.
            spoofer.Verify();
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__NameArgNull__Returns400BadRequest() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Spoofed property returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Spoofed method should never be called; if it is, it always throws.
            spoofer.Setup(x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>()))
                .Throws(new Exception("This method should never be reached."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetRandomPieceByNameAndTime(null, 3);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__NameArgEmpty__Returns400BadRequest() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Spoofed property returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Spoofed method should never be called; if it is, it always throws.
            spoofer.Setup(x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>()))
                .Throws(new Exception("This method should never be reached."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetRandomPieceByNameAndTime(string.Empty, 2);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetRandomPieceByNameAndTime__MinutesArgNotValid__Returns400BadRequest() /* working */ {
            Mock<ITypingStore> spoofer = new();

            // Spoofed property returns the same values as the real property.
            spoofer.Setup(x => x.AllowedTimes)
                .Returns(new int[] { 1, 2, 3, 5, 10 });

            // Spoofed method should never be called; if it is, it always throws.
            spoofer.Setup(x => x.TypeableByNameAndTime(It.IsAny<string>(), It.IsAny<int>()))
                .Throws(new Exception("This method should never be reached."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetRandomPieceByNameAndTime("Text Name", 7);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetRandomPieceByNameAndTime()


        #region GetAllPieces()

        [Fact]
        public async Test GetAllPieces__StoreReturnsTypeables__ReturnsOkResultWithSameTypeables() /* working */ {
            /* Arrange */
            List<Typeable> expecteds = new() {
                new("Typeable 1"),
                new("Typeable 2"),
                new("Typeable 3")
            };

            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTypeables())
                .Returns(expecteds);

            TypingController subject = new(spoofer.Object);


            /* Act */
            IActionResult raw = await subject.GetAllPieces();
            OkObjectResult result = raw as OkObjectResult;
            List<Typeable> actuals = result.Value as List<Typeable>;


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        [Fact]
        public async Test GetAllPieces__StoreReturnsEmptySet__ReturnsOkResultWithEmptySet() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTypeables())
                .Returns(new List<Typeable>());

            TypingController subject = new(spoofer.Object);


            /* Act */
            IActionResult raw = await subject.GetAllPieces();
            OkObjectResult result = raw as OkObjectResult;
            List<Typeable> actual = result.Value as List<Typeable>;


            /* Assert */
            Try.Empty(actual);
        }

        [Fact]
        public async Test GetAllPieces__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTypeables())
                .Throws(new Exception("Test Exception"));

            int expected = 500;

            TypingController subject = new(spoofer.Object);


            /* Act */
            IActionResult raw = await subject.GetAllPieces();
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetAllPieces()


        #region GetTextByName()

        [Fact]
        public async Test GetTextByName__StoreReturnsTextSource__ReturnsOkWithSameNamedTextSource() /* working */ {
            /* Arrange */
            string expected = "Text Source Name";

            // Spoof generator for dependency.
            Mock<ITypingStore> spoofer = new();

            // Spoofing that the dependency always returns a TextSource with the right name.
            spoofer.Setup(x => x.TextSourceByName(It.IsAny<string>()))
                .Returns(new TextSource(expected, "Text in text source."));

            // Model code, given spoofed dependency.
            TypingController subject = new(spoofer.Object);


            /* Act */
            IActionResult raw = await subject.GetTextByName(expected);


            /* Assemble */
            OkObjectResult result = raw as OkObjectResult;
            TextSource source = result.Value as TextSource;
            string actual = source.Name;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetTextByName__StoreReturnsNull__Returns404NotFound() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Spoofed method always returns null.
            spoofer.Setup(x => x.TextSourceByName(It.IsAny<string>()))
                .Returns<TextSource>(null);

            TypingController subject = new(spoofer.Object);

            int expected = 404;


            /* Act */
            IActionResult raw = await subject.GetTextByName("Name");
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetTextByName__NullArg__Returns400BadRequest() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Should not be reached; if it is, always a throw.
            spoofer.Setup(x => x.TextSourceByName(It.IsAny<string>()))
                .Throws(new Exception("This method should never get called."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetTextByName(null);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetTextByName__EmptyArg__Returns400BadRequest() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Should not be reached; if it is, always a throw.
            spoofer.Setup(x => x.TextSourceByName(It.IsAny<string>()))
                .Throws(new Exception("This method should never get called."));

            TypingController subject = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.GetTextByName(string.Empty);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetTextByName__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // The spoofed method always throws.
            spoofer.Setup(x => x.TextSourceByName(It.IsAny<string>()))
                .Throws(new Exception("Spoofed Exception"));

            TypingController subject = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await subject.GetTextByName("Text Name");
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test GetTextByName__KnownArg__SameArgGivenToStore() /* working */ {
            /* Arrange */
            string expected = "Name Of Text";

            Mock<ITypingStore> spoofer = new();

            // The spoofed method verifies its arg using the lambda.
            spoofer.Setup(x => x.TextSourceByName(It.Is<string>(x => x.Equals(expected))))
                .Verifiable();

            TypingController subject = new(spoofer.Object);


            /* Act */
            await subject.GetTextByName(expected);


            /* Assert */
            // The spoofer's method uses the lambda to check the arg passed.
            spoofer.Verify();
        }

        #endregion GetTextByName()


        #region GetAllTextNames()

        [Fact]
        public async Test GetAllTextNames__StoreReturnsNoTextNames__ReturnsOkObjectWithEmptyList() /* working */ {
            /* Arrange */
            // Initing the spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Setting up the spoof x so its spoofed 
            // method always returns an empty List<T>.
            spoofer.Setup(x => x.AllTextNames())
                .Returns(new List<string>());

            // Initing the model code with the spoof .Object.
            TypingController topic = new(spoofer.Object);


            /* Act */
            IActionResult raw = await topic.GetAllTextNames();
            OkObjectResult result = raw as OkObjectResult;
            List<string> actual = result.Value as List<string>;


            /* Assert */
            Try.Empty(actual);
        }

        [Fact]
        public async Test GetAllTextNames__StoreReturnsTextNames__ReturnsOkObjectWithSameNames() /* working */ {
            /* Arrange */
            List<string> expecteds = new() { "A Name", "Another Name", "A Third Name" };

            // Initing spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Setting dependency method on spoof x 
            // to always return the expected names.
            spoofer.Setup(x => x.AllTextNames())
                .Returns(expecteds);

            // Initing model code with spoof .Object.
            TypingController topic = new(spoofer.Object);


            /* Act */
            IActionResult raw = await topic.GetAllTextNames();
            OkObjectResult result = raw as OkObjectResult;
            object actuals = result.Value as List<string>;


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        [Fact]
        public async Test GetAllTextNames__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            // Initing spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Setting dependency method 
            // on spoof x to always throw.
            spoofer.Setup(x => x.AllTextNames())
                .Throws(new Exception("Some Exception"));

            // Initing model code with spoof .Object.
            TypingController topic = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.GetAllTextNames();
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetAllTextNames()


        #region GetAllTexts()

        [Fact]
        public async Test GetAllTexts__StoreReturnsEmptyList__ReturnsOkObjectWithEmptyList() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTexts())
                .Returns(new List<TextSource>());

            TypingController topic = new(spoofer.Object);

            object expected = null;


            /* Act */
            IActionResult raw = await topic.GetAllTexts();
            OkObjectResult result = raw as OkObjectResult;
            List<TextSource> actual = result.Value as List<TextSource>;


            /* Assert */
            Try.Empty(actual);
        }

        [Fact]
        public async Test GetAllTexts__StoreReturnsTextSources__ReturnsOkObjectWithSameTextSources() /* working */ {
            /* Arrange */
            List<TextSource> sources = new() {
                new("First", "This is the first text."),
                new("Second", "This text is second.")
            };

            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTexts())
                .Returns(sources);

            TypingController topic = new(spoofer.Object);

            List<(string name, string text)> expecteds = sources
                .Select(x => (x.Name, x.Text))
                .ToList();


            /* Act */
            IActionResult raw = await topic.GetAllTexts();
            OkObjectResult result = raw as OkObjectResult;
            List<TextSource> results = result.Value as List<TextSource>;

            List<(string name, string text)> actuals = results
                .Select(x => (x.Name, x.Text))
                .ToList();


            /* Assert */
            Try.Equal(expecteds, actuals);
        }

        [Fact]
        public async Test GetAllTexts__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            spoofer.Setup(x => x.AllTexts())
                .Throws(new Exception("Test Exception"));

            TypingController topic = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.GetAllTexts();
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        #endregion GetAllTexts()


        #region AddText()

        [Fact]
        public async Test AddText__StoreDoesNotThrow__Returns200OKStatus() /* working */ {
            /* Arrange */

            // Initing spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Defining the dependency x's target method to take any TextSource arg.
            spoofer.Setup(x => x.AddTextSource(It.IsAny<TextSource>()));

            // Initing model code with the spoofed dependency .Object.
            TypingController topic = new(spoofer.Object);

            int expected = 200;


            /* Act */
            IActionResult raw = await topic.AddText("Some text to be stored.", "Text Name");
            OkResult result = raw as OkResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */

            // Initing spoof generator.
            Mock<ITypingStore> spoofer = new();

            // Defining spoofed dependency method on 
            // spoof x to throw, regardless of input.
            spoofer.Setup(x => x.AddTextSource(It.IsAny<TextSource>()))
                .Throws(new Exception());

            // Initing model code with spoofed .Object.
            TypingController subject = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await subject.AddText("Example text", "Name");
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__TextArgNull__Returns400BadRequestError() /* working */ {
            /* Arrange */
            // No spoofed dependency, because it should never be called.
            TypingController subject = new(null);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.AddText(null, "Name");
            BadRequestResult result = raw as BadRequestResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__TextArgWhitespace__Returns400BadRequestError() /* working */ {
            /* Arrange */
            // No spoofed dependency, because it should never be called.
            TypingController subject = new(null);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.AddText("  ", "Name");
            BadRequestResult result = raw as BadRequestResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__NameArgNull__Returns400BadRequestError() /* working */ {
            /* Arrange */
            // No spoofed dependency, because it should never be called.
            TypingController subject = new(null);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.AddText("Text", null);
            BadRequestResult result = raw as BadRequestResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__NameArgEmpty__Returns400BadRequestError() /* working */ {
            /* Arrange */
            // No spoofed dependency, because it should never be called.
            TypingController subject = new(null);

            int expected = 400;


            /* Act */
            IActionResult raw = await subject.AddText("Text", string.Empty);
            BadRequestResult result = raw as BadRequestResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test AddText__KnownTwoArgs__StoreGivenTextSourceFromArgs() /* working */ {
            /* Arrange */
            (string name, string text) = ("Source Name", "This is the text.");
            TextSource arg = new(name, text);

            // Spoofing the dependency method to confirm its given arg using the lambda.
            Mock<ITypingStore> spoofer = new();
            spoofer.Setup(
                    x => x.AddTextSource(It.Is<TextSource>(x => x.Name == arg.Name && x.Text == arg.Text))
                )
                .Verifiable(
                    "The expected TextSource was not provided to the dependency."
                );

            // The model code, provided the spoof.
            TypingController subject = new(spoofer.Object);


            /* Act */
            await subject.AddText(text, name);


            /* Assert */
            // Use the mock generator's method to confirm its arg.
            spoofer.Verify();
        }

        #endregion AddText()


        #region RemoveText()

        [Fact]
        public async Test RemoveText__StoreDoesntThrow__Returns200OkResult() /* working */ {
            /* Arrange */
            // Initing a spoof generator for the dependency.
            Mock<ITypingStore> spoofer = new();

            // Setting target method on the spoof x so that whatever 
            // the arg passed is, it always return without a throw.
            spoofer.Setup(x => x.RemoveTextSource(It.IsAny<string>()));

            // Initing the model code with the spoof .Object.
            TypingController topic = new(spoofer.Object);

            int expected = 200;


            /* Act */
            IActionResult raw = await topic.RemoveText("Some Name");
            OkResult result = raw as OkResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test RemoveText__StoreThrows__Returns500Error() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Always throws regardless of arg.
            spoofer.Setup(x => x.RemoveTextSource(It.IsAny<string>()))
                .Throws(new Exception("Spoof Exception"));

            TypingController topic = new(spoofer.Object);

            int expected = 500;


            /* Act */
            IActionResult raw = await topic.RemoveText("Some Name");
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test RemoveText__NullArg__Returns400BadRequestError() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Forcing a 500 error if the dependency is called with a null arg.
            spoofer.Setup(x => x.RemoveTextSource(It.Is<string>(x => x == null)))
                .Throws(new Exception("This dependency should not have been reached."));

            TypingController topic = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await topic.RemoveText(null);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test RemoveText__EmptyArg__Returns400BadRequestError() /* working */ {
            /* Arrange */
            Mock<ITypingStore> spoofer = new();

            // Forcing a 500 error if the dependency is called with an empty arg.
            spoofer.Setup(x => x.RemoveTextSource(It.Is<string>(x => x == string.Empty)))
                .Throws(new Exception("This dependency should not have been reached."));

            TypingController topic = new(spoofer.Object);

            int expected = 400;


            /* Act */
            IActionResult raw = await topic.RemoveText(string.Empty);
            StatusCodeResult result = raw as StatusCodeResult;
            int actual = result.StatusCode;


            /* Assert */
            Try.Equal(expected, actual);
        }

        [Fact]
        public async Test RemoveText__KnownArg__DependencyPassedSameArg() /* working */ {
            /* Arrange */
            string expected = "Text Name";

            Mock<ITypingStore> spoofer = new();

            // Setting up the spoof x to check the test condition.
            spoofer.Setup(x => x.RemoveTextSource(It.Is<string>(x => x.Equals(expected))))
                .Verifiable("The arg passed was not the expected one.");

            TypingController topic = new(spoofer.Object);


            /* Act */
            await topic.RemoveText(expected);


            /* Assert */
            // Using the spoofer's feature to check the test condition.
            spoofer.Verify();
        }

        #endregion RemoveText()

    }
}
