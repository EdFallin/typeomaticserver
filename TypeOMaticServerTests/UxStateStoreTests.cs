using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Test = System.Threading.Tasks.Task;

using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    [Collection("Run Serially")]
    public class UxStateStoreTests
    {
        #region Test fixtures

        private async Task DeleteAnySite(string site) /* verified */ {
            // Giving the file system time to be ready.
            await Task.Delay(10);

            // Actually delete the folder if it exists, 
            // as well as any contents it may have.
            if (Directory.Exists(site)) {
                Directory.Delete(site, true);
            }

            // Give the file system time to catch up.
            await Task.Delay(10);
        }

        #endregion Test fixtures


        #region Constructor

        [Fact]
        public void Constructor__NoConditions__RightStoreFileAssembled() /* working */ {
            /* Arrange */
            string appFolder = UxStateStore.RelativeRoot;
            string stateFolder = UxStateStore.Folder;

            string expected = Path.Combine(appFolder, stateFolder, nameof(UxStateStore));
            expected = Path.ChangeExtension(expected, UxStateStore.FileExtension);


            /* Act */
            UxStateStore store = new();


            /* Assemble */
            string actual = store.Subject;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor__NoStoredUxState__DefaultUxStateRetained() /* working */ {
            /* Arrange */
            UxState expected = new UxState {
                IsChooserHidden = false,
                IsTimerHidden = false,
                IsWelcomeHidden = false,
                TypingStyle = "TypeOver"
            };


            /* Act */
            UxStateStore topic = new();


            /* Assemble */
            UxState actual = topic.State;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Constructor__KnownStoredUxState__StoredUxStateRetained() /* working */ {
            /* Arrange */
            UxState expected = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = false,
                IsWelcomeHidden = true,
                TypingStyle = "SomeStyle"
            };

            #region Saving the UxState as JSON at the default site

            string asJson = JsonSerializer.Serialize(expected, typeof(UxState));

            string saveTo = Path.Combine(UxStateStore.RelativeRoot, UxStateStore.Folder);

            if (!Directory.Exists(saveTo)) {
                Directory.CreateDirectory(saveTo);
            }

            string saveAs = Path.Combine(saveTo, nameof(UxStateStore));
            saveAs = Path.ChangeExtension(saveAs, UxStateStore.FileExtension);

            File.WriteAllText(saveAs, asJson);

            #endregion Saving the UxState as JSON at the default site


            /* Act */
            UxStateStore topic = new();


            /* Assemble */
            UxState actual = topic.State;


            /* Clean up */
            await DeleteAnySite(saveTo);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Constructor


        #region Save()

        [Fact]
        public async Test Save__KnownUxState__UxStateIsSaved() /* working */ {
            /* Arrange */
            UxStateStore first = new();

            UxState expected = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = false,
                IsWelcomeHidden = true,
                TypingStyle = "SomeStyle"
            };


            /* Act */
            first.State = expected;
            first.Save();


            /* Assemble */
            UxStateStore second = new();
            UxState actual = second.State;


            /* Clean up */
            await DeleteAnySite(first.Site);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Test Save__ChangedState__ChangedUxStateIsSaved() /* working */ {
            /* Arrange */
            UxStateStore first = new();

            UxState expected = new UxState {
                IsChooserHidden = true,
                IsTimerHidden = false,
                IsWelcomeHidden = true,
                TypingStyle = "SomeStyle"
            };

            first.State = expected;
            first.Save();


            /* Act */
            // Changing and saving again.
            expected.IsTimerHidden = true;
            expected.TypingStyle = "DifferentStyle";
            first.Save();


            /* Assemble */
            UxStateStore second = new();
            UxState actual = second.State;


            /* Clean up */
            await DeleteAnySite(first.Site);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Save()
    }
}
