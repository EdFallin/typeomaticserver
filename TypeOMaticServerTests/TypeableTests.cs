using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using TypeOMaticServer;
using Xunit;

namespace TypeOMaticServerTests
{
    public class TypeableTests
    {
        #region Definitions

        private const int CHARS_A_WORD = 5;

        #endregion Definitions


        #region Constructors

        [Fact]
        public void Constructor__NullOnly__EmptyStringAsName() /* working */ {
            /* This effectively is testing the private method IncipitNameFromText(), 
               which is a dependency of just the no-name constructors. */

            /* Arrange */
            string text = null;

            string expected = string.Empty;


            /* Act */
            Typeable topic = new(text);
            string actual = topic.Name;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor__EmptyStringOnly__EmptyStringAsName() /* working */ {
            /* This effectively is testing the private method IncipitNameFromText(), 
               which is a dependency of just the no-name constructors. */

            /* Arrange */
            string text = null;

            string expected = string.Empty;


            /* Act */
            Typeable topic = new(text);
            string actual = topic.Name;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor__LongTextOnly__FirstFiveWordsAsName() /* working */ {
            /* This effectively is testing the private method IncipitNameFromText(), 
               which is a dependency of just the no-name constructors. */

            /* Arrange */
            string text = "Long text with many more words than five and indeed "
                        + "with many more than one hundred characters overall,"
                        + "perhaps well over that number, perhaps not.";


            /* Pre-test */
            Assert.True(text.Length > 100);

            string expected = "Long text with many more";


            /* Act */
            Typeable topic = new(text);
            string actual = topic.Name;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor__Under100CharsTextOnly__FirstFiveWordsAsName() /* working */ {
            /* This effectively is testing the private method IncipitNameFromText(), 
               which is a dependency of just the no-name constructors. */

            /* Arrange */
            string text = "Medium name with more words than five but under 100 characters.";


            /* Pre-test */
            Assert.True(text.Length < 100);

            string expected = "Medium name with more words";


            /* Act */
            Typeable topic = new(text);
            string actual = topic.Name;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor__Under5WordsTextOnly__AllWordsAsName() /* working */ {
            /* This effectively is testing the private method IncipitNameFromText(), 
               which is a dependency of just the no-name constructors. */

            /* Arrange */
            string text = "Short stretch of text";

            string expected = "Short stretch of text";


            /* Act */
            Typeable topic = new(text);
            string actual = topic.Name;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Constructors


        #region .WordCount

        [Fact]
        public void WordCount__NullText__ReturnsZero() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(null);
            int expected = 0;


            /* Act */
            int actual = topic.WordCount;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WordCount__EmptyText__ReturnsZero() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(string.Empty);
            int expected = 0;


            /* Act */
            int actual = topic.WordCount;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WordCount__KnownTextMultipleOf5__CorrectMultipleOf5Chars() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable("This text's length is 25.");
            int expected = (topic.Text.Length / 5);


            /* Act */
            int actual = topic.WordCount;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WordCount__KnownTextNotMultipleOf5__CorrectMultipleOf5Chars() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable("This text's length isn't an even multiple of 5.");
            int expected = (topic.Text.Length / 5) + (topic.Text.Length % 5 != 0 ? 1 : 0);


            /* Act */
            int actual = topic.WordCount;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion .WordCount


        #region .MinuteLength

        [Fact]
        public void MinuteLength__NullText__Returns0Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(null);
            int expected = 0;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__EmptyText__Returns0Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(string.Empty);
            int expected = 0;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__74Words__Returns1Minute() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('a', 74 * CHARS_A_WORD));
            int expected = 1;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__125Words__Returns1Minute() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('a', 125 * CHARS_A_WORD));
            int expected = 1;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__286Words__Returns2Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('b', 286 * CHARS_A_WORD));
            int expected = 2;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__399Words__Returns3Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('c', 399 * CHARS_A_WORD));
            int expected = 3;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__427Words__Returns3Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('c', 427 * CHARS_A_WORD));
            int expected = 3;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__514Words__Returns5Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('d', 514 * CHARS_A_WORD));
            int expected = 5;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__736Words__Returns5Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('e', 736 * CHARS_A_WORD));
            int expected = 5;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void MinuteLength__1021Words__Returns10Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('e', 1021 * CHARS_A_WORD));
            int expected = 10;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MinuteLength__1267Words__Returns10Minutes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable(new string('e', 1267 * CHARS_A_WORD));
            int expected = 10;


            /* Act */
            int actual = topic.MinuteLength;


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion .MinuteLength


        #region Equals()

        [Fact]
        public void Equals__NotEqual__ReturnsFalse() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            // The text is different.
            Typeable other = new Typeable {
                Name = "One",
                Text = "One two three four five eight seven six nine ten."
            };

            bool expected = false;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equals__AreEqual__ReturnsTrue() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            Typeable other = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            bool expected = true;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equals__DifferentClass__ReturnsFalse() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            object other = new List<string>();

            bool expected = false;


            /* Act */
            bool actual = topic.Equals(other);


            /* Assert */
            Assert.Equal(expected, actual);
        }

        #endregion Equals()


        #region GetHashCode()

        [Fact]
        public void GetHashCode__EqualContents__SameCode() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            Typeable other = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            int expected = topic.GetHashCode();


            /* Act */
            int actual = other.GetHashCode();


            /* Assert */
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetHashCode__DifferentContents__DifferentCodes() /* working */ {
            /* Arrange */
            Typeable topic = new Typeable {
                Name = "One",
                Text = "One two three four five six seven eight nine ten."
            };

            Typeable other = new Typeable {
                Name = "Two",
                Text = "One two three four five six seven eight nine ten eleven."
            };

            int expected = topic.GetHashCode();


            /* Act */
            int actual = other.GetHashCode();


            /* Assert */
            Assert.NotEqual(expected, actual);
        }

        #endregion GetHashCode()

    }
}