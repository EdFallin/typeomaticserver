using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace TypeOMaticServer
{
    /// <summary>
    /// A trivial wrapper for a StatusCodeResult that always uses 
    /// the status code for a processing failure at the server end.
    /// </summary>
    public class ServerEndFailResult : StatusCodeResult
    {
        public ServerEndFailResult() : base(500) {
            /* No operations. */
        }
    }
}
