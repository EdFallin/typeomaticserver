using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace TypeOMaticServer {
    public interface ITypingStore
    {
        #region Properties

        /// <summary>
        /// The number of minutes Typeables may have.
        /// </summary>
        int[] AllowedTimes { get; }

        #endregion Properties


        #region Adding and removing TextSources

        public void AddTextSource(TextSource subject);
        public void RemoveTextSource(string name);

        #endregion Adding and removing TextSources


        #region Getting Typeables

        public List<Typeable> AllTypeables();
        public Typeable RandomTypeableByTime(int minutes);
        public Typeable TypeableByNameAndTime(string name, int minutes);

        #endregion Getting Typeables


        #region Getting TextSources

        public List<string> AllTextNames();
        public List<TextSource> AllTexts();
        public TextSource TextSourceByName(string name);
        public TextSource TextSourceByWfi(string wfi);
        
        #endregion Getting TextSources

    }
}