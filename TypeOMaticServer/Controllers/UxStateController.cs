using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TypeOMaticServer
{
    [ApiController]
    [Route("[controller]")]
    public class UxStateController : ControllerBase
    {
        private IUxStateStore _store = null;

        public UxStateController(IUxStateStore store) /* verified */ {
            _store = store;
        }

        [HttpGet]
        [ProducesResponseType(typeof(UxState), 200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get() /* passed */ {
            try {
                UxState state = _store.State;

                OkObjectResult result = new OkObjectResult(state);
                return result;
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] UxState state) /* passed */ {
            try {
                _store.State = state;
                _store.Save();
                return new OkResult();
            }
            catch {
                return new ServerEndFailResult();
            }
        }
    }
}