using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeOMaticServer
{
    public interface IUxStateStore
    {
        UxState State { get; set; }
        void Save();
    }
}