using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TypeOMaticServer
{
    [ApiController]
    /* Inserts the controller's name, minus "Controller", into the URL, if other Route attributes are done correctly. */
    [Route("[controller]")]
    public class TypingController : ControllerBase {
        #region Fields

        private ITypingStore _store;

        #endregion Fields


        #region Constructors

        public TypingController(ITypingStore store) /* verified */ {
            _store = store;
        }

        #endregion Constructors


        #region Getting minutes

        [HttpGet]
        [ProducesResponseType(typeof(int[]), 200)]
        [ProducesResponseType(500)]
        /* Inserts the chosen text into the URL after the controller's reduced name, _if_ there's no leading "/". */
        [Route("MinutesPossible")]
        public async Task<IActionResult> GetMinutesPossible() /* passed */ {
            try {
                return new OkObjectResult(_store.AllowedTimes);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        #endregion Getting minutes


        #region Getting Typeables (pieces)

        [HttpGet]
        [ProducesResponseType(typeof(Typeable), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        /* Inserts the chosen route into the URL after the controller's reduced name _if_ there's 
         * no leading "/", and passes the URL value in the braced segment to the method parameter 
         * with the same name, but only if the names match exactly, including capitalization. */
        [Route("Random/{minutes}")]
        public async Task<IActionResult> GetRandomPieceByTime(int minutes) /* passed */ {
            try {
                if (!_store.AllowedTimes.Contains(minutes)) {
                    return new BadRequestResult();
                }

                Typeable result = _store.RandomTypeableByTime(minutes);

                if (result == null) {
                    return new NotFoundResult();
                }

                return new OkObjectResult(result);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(Typeable), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Route("Random/{name}/{minutes}")]
        public async Task<IActionResult> GetRandomPieceByNameAndTime(string name, int minutes) /* passed */ {
            try {
                // Fail path: bad args.
                if (string.IsNullOrWhiteSpace(name) || !_store.AllowedTimes.Contains(minutes)) {
                    return new BadRequestResult();
                }

                // Main path.
                Typeable result = _store.TypeableByNameAndTime(name, minutes);

                if (result == null) {
                    return new NotFoundResult();
                }

                return new OkObjectResult(result);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<Typeable>), 200)]
        [ProducesResponseType(500)]
        [Route("All/Pieces")]
        public async Task<IActionResult> GetAllPieces() /* passed */ {
            try {
                // An empty list is not a 404.
                List<Typeable> typeables = _store.AllTypeables();
                return new OkObjectResult(typeables);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        #endregion Getting Typeables (pieces)


        #region Getting TextSources (texts)

        [HttpGet]
        [ProducesResponseType(typeof(TextSource), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Route("WholeNamed/{name}")]
        public async Task<IActionResult> GetTextByName(string name) /* passed */ {
            try {
                if (string.IsNullOrWhiteSpace(name)) {
                    return new BadRequestResult();
                }

                TextSource result = _store.TextSourceByName(name);

                if (result == null) {
                    return new NotFoundResult();
                }

                return new OkObjectResult(result);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(500)]
        [Route("All/Names")]
        public async Task<IActionResult> GetAllTextNames() /* passed */ {
            try {
                // An empty list is not a 404.
                List<string> names = _store.AllTextNames();
                return new OkObjectResult(names);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<TextSource>), 200)]
        [ProducesResponseType(500)]
        [Route("All/Texts")]
        public async Task<IActionResult> GetAllTexts() /* passed */ {
            try {
                // An empty list is not a 404.
                List<TextSource> texts = _store.AllTexts();
                return new OkObjectResult(texts);
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        #endregion Getting TextSources (texts)


        #region Adding and removing TextSources (texts)

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("Add/{name}")]
        public async Task<IActionResult> AddText([FromBody] string text, string name) /* passed */ {
            try {
                if (string.IsNullOrWhiteSpace(text) || string.IsNullOrWhiteSpace(name)) {
                    return new BadRequestResult();
                }

                TextSource toAdd = new TextSource(name, text);
                _store.AddTextSource(toAdd);
                return new OkResult();
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("Delete/{name}")]
        public async Task<IActionResult> RemoveText(string name) /* passed */ {
            try {
                if (string.IsNullOrWhiteSpace(name)) {
                    return new BadRequestResult();
                }

                _store.RemoveTextSource(name);
                return new OkResult();
            }
            catch {
                return new ServerEndFailResult();
            }
        }

        #endregion Adding and removing TextSources (texts)

    }
}