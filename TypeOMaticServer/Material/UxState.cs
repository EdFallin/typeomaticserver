using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeOMaticServer
{
    public class UxState
    {
        #region Properties

        public bool IsWelcomeHidden { get; set; }
        public bool IsTimerHidden { get; set; }
        public bool IsChooserHidden { get; set; }

        /// <summary>
        /// The browser end of the app uses an enum of string type, and the value is not addressed 
        /// at this server end, so I'm just storing it as a string here.
        /// <para/>
        /// Possible values are ReadAndType, TypeOver, and TypeNew.  The default is TypeOver.
        /// </summary>
        public string TypingStyle { get; set; } = "TypeOver";

        #endregion Properties


        #region Overrides: Equals(), GetHashCode()

        public override bool Equals(object obj) /* passed */ {
            if (obj is UxState other) {
                // Is a UxState, so all properties must be equal.
                bool isEqual =
                this.IsChooserHidden == other.IsChooserHidden
                &&
                this.IsTimerHidden == other.IsTimerHidden
                &&
                this.IsWelcomeHidden == other.IsWelcomeHidden
                &&
                this.TypingStyle == other.TypingStyle;

                return isEqual;
            }

            // Not a UxState.
            return false;
        }

        public override int GetHashCode() /* passed */ {
            // Non-overlapping values for the booleans.
            int chooserHash = IsChooserHidden ? 2 : 1;
            int timerHash = IsTimerHidden ? 8 : 4;
            int welcomeHash = IsWelcomeHidden ? 32 : 16;

            // Unique values for each combination of contents.
            int hashFlags = chooserHash + timerHash + welcomeHash;
            int hash = hashFlags * TypingStyle.GetHashCode();

            // To caller.
            return hash;
        }
        
        #endregion Overrides: Equals(), GetHashCode()
    }
}