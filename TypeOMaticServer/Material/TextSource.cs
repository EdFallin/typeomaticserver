using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace TypeOMaticServer
{

    /* The idea here is that there are a bunch of files, and each one is a TextSource. 
       Each TextSource of more than 100 words has multiple segments, each one starting 
       at the beginning of a paragraph, of a given number of words, figuring 100 to a minute.  
       Each segment can be returned as a separate Typeable. */

    public class TextSource
    {
        #region Definitions

        private readonly int[] CHAR_LENGTHS = { 500, 1000, 1500, 2500, 5000 };

        #endregion Definitions


        #region Properties

        public string Name { get; }
        public string Text { get; }

        #endregion Properties


        #region Constructors

        public TextSource(string name, string text) {
            Name = name;
            Text = text;
        }

        #endregion Constructors


        #region AsTypeables()

        public List<Typeable> AsTypeables() /* passed */ {
            /* Future: Parse the text at each paragraph start, with segments 
               for each of the possible lengths (as long as is possible). */

            List<Typeable> results = new();

            StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries;
            string[] paragraphs = Text.Split(Environment.NewLine, options);

            // Trying to build all lengths possible from each paragraph.
            for (int p = 0; p < paragraphs.Length; p++) {
                // Building all possible lengths starting at the current paragraph.
                foreach (int total in CHAR_LENGTHS) {
                    // Current paragraph is always included.
                    StringBuilder b = new(paragraphs[p]);
                    int n = p + 1;

                    // Later paragraphs added until segment passes target length.
                    // Original paragraphs are merged into one long text block.
                    while (b.Length < total && n < paragraphs.Length) {
                        b.Append(' ');
                        b.Append(paragraphs[n]);
                        n++;
                    }

                    // If not as much text as targeted, the segment is skipped.
                    if (b.Length < total) {
                        break;
                    }

                    // From text segment to Typeable with incipit as name.
                    Typeable t = new(b.ToString());
                    results.Add(t);
                }
            }

            return results;
        }

        #endregion AsTypeables() 
    }
}