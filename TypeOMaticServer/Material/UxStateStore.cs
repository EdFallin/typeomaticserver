using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO;

namespace TypeOMaticServer
{
    public class UxStateStore : IUxStateStore
    {
        #region Definitions

        public const string RelativeRoot = @".";
        public const string Folder = @"State";

        public const string FileExtension = ".json";

        #endregion Definitions


        #region Fields and properties

        private string _site;
        private string _file;
        public string Site { get => _site; }
        public string Subject { get => _file; }

        private UxState _state = new();

        public UxState State { get { return _state; } set { _state = value; } }

        #endregion Fields and properties


        #region Constructors and dependencies

        public UxStateStore() /* passed */ {
            CalculateSiteAndPath();

            // Use the default if no JSON file was stored.
            if (!File.Exists(_file)) {
                return;
            }

            RetainUxStateFromFile();
        }

        private void CalculateSiteAndPath() /* verified */ {
            // Path to the JSON file.
            _site = Path.Combine(RelativeRoot, Folder);

            // The JSON file itself.
            _file = Path.Combine(_site, nameof(UxStateStore));
            _file = Path.ChangeExtension(_file, FileExtension);
        }

        private void RetainUxStateFromFile() /* verified */ {
            // Main path: Use the stored JSON file for the state.
            string asJson = File.ReadAllText(_file);
            _state = JsonSerializer.Deserialize(asJson, typeof(UxState)) as UxState;
        }

        #endregion Constructors and dependencies


        #region Methods

        public void Save() /* passed */ {
            // Ensure the site exists for the JSON file.
            if (!Directory.Exists(_site)) {
                Directory.CreateDirectory(_site);
            }

            // Actually save the UxState as a JSON file.
            string asJson = JsonSerializer.Serialize(_state, typeof(UxState));
            File.WriteAllText(_file, asJson);
        }

        #endregion Methods
    }

}