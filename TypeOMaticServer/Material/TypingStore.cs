using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace TypeOMaticServer
{
    /* The idea here is that there are a bunch of files, and each one is a TextSource. 
       Each TextSource of more than 100 words has multiple segments, each one starting 
       at the beginning of a paragraph, of a given number of words, figuring 100 to a minute.  
       Each segment can be returned as a separate Typeable. */

    public class TypingStore : ITypingStore
    {
        #region Definitions

        public const string RelativeRoot = @".";
        public const string Folder = @"Texts";

        public const string FileExt = @"text";

        private const int REPEAT_GAP = 4;

        #endregion Definitions


        #region Fields and properties

        private string _site;

        /// <summary>
        /// The location of the files used as text sources.
        /// </summary>
        public string Site { get => _site; }

        private List<Typeable> _lastFewTypeables = new(REPEAT_GAP);

        public int[] AllowedTimes { get; } = { 1, 2, 3, 5, 10 };

        #endregion Fields and properties


        #region Constructors and dependencies

        public TypingStore() /* passed */ {
            // Calculate a path relative to the app for all files.
            _site = Path.Combine(RelativeRoot, Folder);

            // Make the path if it doesn't exist.
            CreateAnySite();
        }

        private void CreateAnySite() /* verified */ {
            if (!Directory.Exists(_site)) {
                Directory.CreateDirectory(_site);
            }
        }

        #endregion Constructors and dependencies


        #region Adding and removing TextSources

        public void AddTextSource(TextSource subject) /* passed */ {
            string toSave = CalculateSourceWfi(subject.Name);
            File.WriteAllText(toSave, subject.Text);
        }

        public void RemoveTextSource(string name) /* passed */ {
            string toDelete = CalculateSourceWfi(name);

            if (!File.Exists(toDelete)) {
                return;
            }

            File.Delete(toDelete);
        }

        #endregion Adding and removing TextSources


        #region Getting Typeables and dependencies

        public List<Typeable> AllTypeables() /* passed */ {
            List<TextSource> sources = AllTexts();

            List<Typeable> typeables = sources
                .SelectMany(x => x.AsTypeables())
                .ToList();

            return typeables;
        }

        public Typeable RandomTypeableByTime(int minutes) /* passed */ {
            /* Algorithm: A random file is read and looked in randomly 
             * until a typeable is found that hasn't been used recently. 
             * If none, these steps are repeated until all files are tried. */

            Typeable target = null;

            // Get all the TextSource files.
            List<string> files = GetAllTextSourceWfis();

            Random r = new();

            // Look through available files as long as needed and possible.
            while (target == null && files.Count > 0) {
                string wfi = GetNextRandomWfiFrom(files, r);
                TextSource source = TextSourceByWfi(wfi);
                target = GetAnyNewRandomTypeableFrom(source, r, minutes);
            }

            // Tracking allows preceding code to prevent quick repeats.
            KeepTrackOfLatestTypeables(target);

            // Output.
            return target;
        }

        public Typeable TypeableByNameAndTime(string name, int minutes) /* passed */ {
            // Retrieving source, which is null if no such file.
            TextSource source = TextSourceByName(name);

            // Fail path.
            if (source == null) {
                return null;
            }

            // Main path.

            // Get a Typeable that isn't a recent repeat.
            Random r = new();
            Typeable target = GetAnyNewRandomTypeableFrom(source, r, minutes);

            // Save the Typeable to avoid repeating it soon.
            KeepTrackOfLatestTypeables(target);

            // Output.
            return target;
        }

        #endregion Getting Typeables and dependencies


        #region Getting TextSources and their names

        public List<string> AllTextNames() /* passed */ {
            // Get any TextSource files names.
            List<string> files = GetAllTextSourceWfis();

            // A TextSource's name is the same as the plain 
            // name of its file.  Names are alphabetized.
            files = files
                .Select(x => Path.GetFileNameWithoutExtension(x))
                .OrderBy(x => x)
                .ToList();

            return files;
        }

        public List<TextSource> AllTexts() /* passed */ {
            List<TextSource> sources = new();

            // Get all the TextSource files.
            List<string> files = GetAllTextSourceWfis();

            // Convert each file's contents to a TextSource.
            foreach (string file in files) {
                TextSource source = TextSourceByWfi(file);
                sources.Add(source);
            }

            // Alphabetize.
            sources = sources
                .OrderBy(x => x.Name)
                .ToList();

            // Output.
            return sources;
        }

        public TextSource TextSourceByName(string name) /* passed */ {
            string wfi = CalculateSourceWfi(name);
            return TextSourceFromNameAndWfi(name, wfi);
        }

        /// <summary>
        /// WFI = Windows File Indicator, meaning path + name + extension.
        /// </summary>
        public TextSource TextSourceByWfi(string wfi) /* passed */ {
            string name = Path.GetFileNameWithoutExtension(wfi);
            return TextSourceFromNameAndWfi(name, wfi);
        }

        #endregion Getting TextSources and their names


        #region Private dependencies

        private string GetNextRandomWfiFrom(List<string> files, Random r) /* verified */ {
            // Get some next file, but not always in order.
            int next = r.Next();
            next %= files.Count;

            // Each file is used only once.
            string wfi = files[next];
            files.RemoveAt(next);

            return wfi;
        }

        private Typeable GetAnyNewRandomTypeableFrom(TextSource source, Random r, int minutes) /* verified */ {
            Typeable target = null;

            // Get just the target-sized Typeables from the TextSource.
            List<Typeable> typeables = source.AsTypeables()
                .Where(x => x.MinuteLength == minutes)
                .ToList();

            // Try repeatedly within the same file, not repeating 
            // any recent ones of any length with the same incipit.
            while (target == null && typeables.Count > 0) {
                Typeable maybe = GetNextRandomTypeableFrom(typeables, r);

                if (!_lastFewTypeables.Exists(x => x.Name == maybe.Name)) {
                    target = maybe;
                }
            }

            return target;
        }

        private Typeable GetNextRandomTypeableFrom(List<Typeable> typeables, Random r) /* verified */ {
            // Setting up randomness.
            int of = r.Next();
            of %= typeables.Count;

            // Each Typeable is only tried once.
            Typeable typeable = typeables[of];
            typeables.RemoveAt(of);

            // Output.
            return typeable;
        }

        private void KeepTrackOfLatestTypeables(Typeable target) /* verified */ {
            // No-work path.
            if (target == null) {
                return;
            }

            // Main path.
            _lastFewTypeables.Add(target);
            if (_lastFewTypeables.Count > REPEAT_GAP) {
                _lastFewTypeables.RemoveAt(0);
            }
        }

        /// <summary>
        /// WFI = Windows File Indicator, meaning path + name + extension.
        /// </summary>
        private TextSource TextSourceFromNameAndWfi(string name, string wfi) /* verified */ {
            // Fail path.
            if (!File.Exists(wfi)) {
                return null;
            }

            // Main path.
            string text = File.ReadAllText(wfi);

            TextSource source = new(name, text);
            return source;
        }

        /// <summary>
        /// WFI = Windows File Indicator, meaning path + name + extension.
        /// </summary>
        private List<string> GetAllTextSourceWfis() /* verified */ {
            List<string> wfis = Directory
                .GetFiles(_site)
                .ToList();

            return wfis;
        }

        /// <summary>
        /// WFI = Windows File Indicator, meaning path + name + extension.
        /// </summary>
        private string CalculateSourceWfi(string name) /* verified */ {
            string site = _site;
            site = Path.Combine(site, name);
            site = Path.ChangeExtension(site, TypingStore.FileExt);

            return site;
        }

        #endregion Private dependencies

    }
}