using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace TypeOMaticServer
{
    public class Typeable
    {
        #region Definitions

        private const int WORDS_A_MINUTE = 100;

        #endregion Definitions


        #region Properties

        #region Stored

        public string Name { get; set; }
        public string Text { get; set; }

        #endregion Stored


        #region Calculated

        public int WordCount /* passed */ {
            get {
                if (string.IsNullOrWhiteSpace(Text)) {
                    return 0;
                }

                // Word count is number of 5-letter segments in the text.
                int count = Text.Length / 5;

                // Plus one for any final partial segment.
                if (Text.Length % 5 != 0) {
                    count += 1;
                }

                return count;
            }
        }

        public int MinuteLength /* passed */ {
            get {
                // Edge case, unlikely except in tests.
                if (WordCount == 0) {
                    return 0;
                }

                // Length is based on 100 words a minute.
                int minutes = WordCount / WORDS_A_MINUTE;

                // But it must be one of 1, 2, 3, 5, or 10.
                if (minutes < 1) { return 1; }
                if (3 < minutes && minutes < 5) { return 3; }
                if (5 < minutes && minutes < 10) { return 5; }
                if (10 < minutes) { return 10; }

                // The calculated length was already one of the predefined numbers.
                return minutes;
            }
        }

        #endregion Calculated

        #endregion Properties


        #region Constructors and dependencies

        public Typeable() /* ok */ {
            /* No operations. */
        }

        public Typeable(string text) /* passed */ {
            Text = text;
            Name = IncipitNameFromText(Text);
        }

        public Typeable(string name, string text) /* ok */ {
            Name = name;
            Text = text;
        }

        private string IncipitNameFromText(string text) /* verified */ {
            if (string.IsNullOrWhiteSpace(text)) {
                return string.Empty;
            }

            string incipit = text.Substring(0, Math.Min(text.Length, 100));
            string[] incipitWords = text.Split(' ');
            incipitWords = incipitWords
                .Take(5)
                .ToArray();

            incipit = string.Join(' ', incipitWords);

            return incipit;
        }

        #endregion Constructors and dependencies


        #region Overrides: Equals(), GetHashCode()

        public override bool Equals(object obj) /* passed */ {
            if (obj is Typeable other) {
                return
                    this.Name == other.Name
                    &&
                    this.Text == other.Text;
            }

            return false;
        }

        public override int GetHashCode() /* passed */ {
            int hash = Name.GetHashCode() * Text.GetHashCode();
            return hash;
        }

        #endregion Overrides: Equals(), GetHashCode()

    }
}